package main

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var db *sqlx.DB

func route() *chi.Mux {
	r := chi.NewRouter()

	// set middlewares
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		response, err := json.Marshal(map[string]interface{}{
			"status": "ok",
		})

		// set response header
		w.Header().Set("Content-Type", "application/json")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(`{"status": "not ok"}`))
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write(response)
	})

	r.Route("/articles", func(articleRouter chi.Router) {
		articleRouter.Post("/", createHandler)
		articleRouter.Get("/", readHandler)
		articleRouter.Put("/{articleID}", updateHandler)
		articleRouter.Delete("/{articleID}", deleteHandler)
	})
	return r
}

func main() {
	// setup router
	r := route()

	// setup db connection
	// db is global var, see above
	db = connectDB()
	defer db.Close()

	http.ListenAndServe(":3456", r)
}

func connectDB() *sqlx.DB {
	db, err := sqlx.Connect("mysql", "root:root@tcp(localhost:3306)/myblog?parseTime=true")
	if err != nil {
		panic(err)
	}

	return db
}
