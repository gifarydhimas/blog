package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
)

func createHandler(w http.ResponseWriter, r *http.Request) {
	// receive request

	var article Article

	// decode request into struct
	if err := json.NewDecoder(r.Body).Decode(&article); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(&map[string]string{
			"message": "Invalid request body",
		})
		return
	}

	tx := db.MustBegin()
	res, err := tx.NamedExec("INSERT INTO articles (title, content, author) VALUES(:title, :content, :author)", &article)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(&map[string]string{
			"message": "Oops, unable to save new article",
		})
		return
	}

	if err := tx.Commit(); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(&map[string]string{
			"message": "Oops, unable to save new article",
		})
		return
	}

	lastInsertID, _ := res.LastInsertId()
	article.ID = int(lastInsertID)

	// serve response
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&article)
}

func readHandler(w http.ResponseWriter, r *http.Request) {
	var articles []Article

	if err := db.Select(&articles, "SELECT * FROM `articles`"); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(&map[string]string{
			"message": "Oops, unable to fetch articles",
		})
		return
	}

	// serve response
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&articles)
}

type updateArticle struct {
	Title   *string `json:"title,omitempty"`
	Content *string `json:"content,omitempty"`
	Author  *string `json:"author,omitempty"`
}

func updateHandler(w http.ResponseWriter, r *http.Request) {
	articleID := chi.URLParam(r, "articleID")
	var (
		article Article
		update  updateArticle
	)

	// decode request into struct
	if err := json.NewDecoder(r.Body).Decode(&update); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(&map[string]string{
			"message": "Invalid body request",
		})
		return
	}

	// fetch current article
	if err := db.Get(&article, "SELECT * FROM `articles` WHERE `id` = ?", articleID); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(&map[string]string{
			"message": "Oops, unable to update article",
		})
		return
	}

	if update.Author != nil {
		article.Author = *update.Author
	}
	if update.Title != nil {
		article.Title = *update.Title
	}
	if update.Content != nil {
		article.Content = *update.Content
	}

	// apply the update
	if _, err := db.NamedExec("UPDATE `articles` SET `title` = :title, `content` = :content, `author` = :author WHERE `id` = :id", &article); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(&map[string]string{
			"message": "Oops, unable to update article",
		})
		return
	}

	// serve response
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&article)

}

func deleteHandler(w http.ResponseWriter, r *http.Request) {
	articleID := chi.URLParam(r, "articleID")
	// var article Article

	if _, err := db.Exec("DELETE FROM `articles` WHERE id = ?", articleID); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(&map[string]string{
			"message": "Oops, unable to delete article",
		})
		return
	}

	w.WriteHeader(http.StatusNoContent)
	// w.Header().Set("Content-Type", "application/json")
}
