# blog

## dependencies

- github.com/jmoiron/sqlx
- github.com/go-chi/chi
- github.com/go-sql-driver/mysql

Install using `go get`:

```
go get github.com/jmoiron/sqlx
go get github.com/go-chi/chi
go get github.com/go-sql-driver/mysql
```

## how to run

`go run *.go`

## database

DSN:

```
root:root123@tcp(localhost:3306)/myblog?parseTime=true
```

| field             | description             |
| ----------------- | ----------------------- |
| `root`            | user                    |
| `root123`         | pass                    |
| `localhost`       | host                    |
| `3306`            | port                    |
| `myblog`          | database name           |
| `?parseTime=true` | options. leave it there |

```sql
CREATE TABLE `articles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text,
  `author` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
```
